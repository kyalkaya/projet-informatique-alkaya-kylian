import random
import copy
def importer_mots(nom_fichier):# cette fonction prend en argument un fichier et retourne une liste des mots(d'au moins 3 caractères) contenue dans le fichier.
        fichier=open(nom_fichier) #ouvre le fichier 
        listedemot=[] #creation d'une liste vide qu'on va mettre à jour avec les mot du fichier nom_fichier
        for i in fichier: #parcours du fichier ligne par ligne (sachant que le fichier mots.txt contient un mot par ligne)
                lignesansretour=i.strip()#on initialise la variable lignesansretour avec la commande i.strip() qui correspond à la ligne i du fichier sans compter le retour à la ligne. Or on sait que dans le fichier 'mots.txt' il y a un mot par ligne donc cela veut dire qu'on initialise lignesansretour avec le mot de la ligne i du fichier.
                if len(lignesansretour)>=3: 
                        listedemot.append(lignesansretour.upper()) # si la ligne fait plus ou est égale à 3 caractère , on ajoute la ligne (qu'on convertie en majuscule à l'aide de upper()) à la  liste listedemot
        return listedemot #on retourne la liste listedemot qui est mise à jour comme il le faut

def choisir_mot_alea(listedemot):#fonction qui permet de choisir un mot aléatoire dans une liste listedemot
        mot_myst=listedemot[random.randint(0,len(listedemot)-1)] #à l'aide du module random , on choisit un mot aléatoire dans la liste listedemot (qui on suppose est mis à jour grace à la fonction 
                                                                        # importer_mots, on choisit un mot entre le premier et dernier mot de la liste inclus
        return mot_myst                                    #on retourne le mot aléatoire choisit dans la liste
        
        
        
def initialiser_mot_part_decouv(mot_myst,car_subst="-"): #cette fonction permet d'initialiser le mot_myst(on suppose qu'il est choisit grâce à la fonction initialiser_mot_part_decouv)
                                                                # le but est de convertir le mot mystère en une liste de caractère ou on a en position 0 et len(mot_myst)-1 , la première et la dernière                                                                        #lettre du mot mystère et mettre un caractère de substitution (par défaut "-") entre la première et dernière lettre.
        liste_de_caractere=[]#création d'une liste vide qu'on mettra à jour
        i = 0 #initialisation de l'indice 
        while i <len(mot_myst):
                if i == 0 or i ==len(mot_myst)-1: #si on atteint la première ou dernière position du mot
                        liste_de_caractere.append(mot_myst[i].upper()) #on ajoute à la liste liste_de_caractère la première ou dernière lettre
                else:
                        liste_de_caractere.append(car_subst)    #sinon on ajoute le caractère de substitution
                i += 1
        return liste_de_caractere #on retourne la liste mise à jour avec le mot mystère qui est initialisé comme on le voulait (exemple si on avait mis le mot arbre , on aura en retour [A,-,-,-,E]
        
        
        
        



def afficher_potence_texte(nb_err,nb_err_max): #le but de cette fonction est d'écrire progressivement le mot "PERDU!".Si nb_err_max est supérieur à 5 alors on va compléter avec des "!" sans compter le point d'exclamation finale . Par exemple avec le nb_err_max égale à 8 et avec une erreur commise , on aura "P-------!"
        mot_perdu = "PERDU"+("!"*(nb_err_max-5))#on initiale une variable mot_perdu avec le mot perdu et le nombre de point d'exclamation selon le nombre d'erreur max"
        perdu = "" #on initiale à vide une variable perdu qu'on mettra à jour.
        if nb_err==0:
                perdu = "-"*(nb_err_max-nb_err)+"!" #quand on aura 0 erreur commise , on aura juste à écrire les tirets avec le point d'exclamation finale.
        elif nb_err>0 and nb_err <= nb_err_max: 
                for i in range(nb_err):
                        perdu = perdu + mot_perdu[i] # si on a commit des erreurs, on modifiera la variable perdu en parcourant la variable mot_perdu et on ajoutera les caractères selon le nombre d'erreur commise.
                perdu = perdu + "-"*(nb_err_max-nb_err)+"!"#on rajoute ensuite les tirets restant + le point d'exclamation finale.
                
        print(perdu)#affiche la potence



def demander_proposition(deja_dit):# on prend en argument une liste de lettre déja dit dans la partie en cours et le but est de retourner une lettre de l'alphabet en majuscule et qui n'a jamais été                                           #dites durant la partie  
        print(deja_dit)#on affiche la liste de lettre déja dite
        liste_alphabet=fabrique_liste_alphabet()#on initialise une liste de lettre de l'alphabet qui nous servira 2 ligne en dessous
        lettre=input('lettre')#on initialise une variable lettre qu'on modifie à l'aide de la fonction input qui nous demande d'écrire une chaîne de caractère (en l'occurence une lettre)
        while lettre.upper() in deja_dit or lettre.upper() not in liste_alphabet:#tant qu'on ecrit une lettre qui est déja dite ou une chaîne de caractère qui n'est pas une lettre (exemple "&"),on                                                                                            #va recommencer jusqu'a qu'on aura une chaîne de caractère qui est une lettre de l'alphabet et qui n'est pas déja                                                                                               #dit
                lettre=input("Vous avez sois deja dit cette lettre ou alors le caractère utilisé n'est pas une lettre de l'alphabet.Veuillez recommencez avec une lettre que vous n'avez jamais utilisé")
        return lettre.upper()#on retourne la lettre majusucule qui remplis les bonne condition




def decouvrir_lettre(lettre,mot_myst,lmot_decouv):# cette fonction prend en argument une lettre choisis durant la partie , le mot mystère et le mot partiellement decouvert lmot_decouv.ON modifie la liste lmot_decouv selon si la lettres choisit correspond à une lettre du mot.Si on parvient à trouver une lettre qui se trouve à une plusieur emplacement du mots , on modifie la liste par effet de bord et on retourne le booléen True. Sinon il n'y a pas de modification et on retourne le booléen False.
        dedans = False#on initialise la variable "dedans" à False
        for i in range(1,len(mot_myst)-1):#On parcourt le mot mystère caractère par caractère du 2ème à l'avant dernière place et non de la première à la dernière place car on connait le premier et le dernier caractère lors de la partie.
                if mot_myst[i].upper()==lettre.upper():#si on tombe sur une lettre correspondant à une lettre du mot mystère lors du parcourt du mot mystère
                        lmot_decouv.pop(i)#on enlève le caractère de substitution du mot partiellement découvert
                        lmot_decouv.insert(i,lettre.upper())#on remplace le caractère de substitution par la lettre trouvé (on écrit la lettre en majuscule)
                        dedans=True#Vu qu'on aura trouvé une lettres correspondante , on changera la valeur de dedans qui vaudra alors True
        return dedans #on retourne un booléen
def partie_humain(mot_myst,nb_err_max,car_subst="-"):#Cette fonction est la fonction qui permet de faire une partie humain contre robot.A l'aide des fonction précédente , on va effectuer une partie de pendu.
        nb_err=0 #il s'agit des erreur faites durant la partie. On initialise à 0
        liste_mot_myst=list(mot_myst.upper()) #on convertie le mot mystère en liste de caractère
        mot_a_decouvrir=initialiser_mot_part_decouv(mot_myst,car_subst)#mot de depart qu'on initialise à l'aide de la fonction initialiser_mot_part_decouv qu'on a crée précédemment
        deja_dit=[]#Liste vide de lettre déja dit qu'on mettra à jour
        print(mot_a_decouvrir)#On écrit le mot à découvrir initialisé
        while nb_err<nb_err_max and liste_mot_myst!=mot_a_decouvrir:#Tant que la partie est pas fini , cet à dire tant qu'on a pas éteins le nombre d'erreur max autorisé ou tant qu'on trouve pas le mot à decouvrir, on joue .
                lettre=demander_proposition(deja_dit)#on écrit une lettre de l'alphabet qui n'a pas été déja dit.
                deja_dit.append(lettre)#on met à jour la liste de lettre déja dit en ajoutant la lettre qu'on a écrit précédent
                lettre_en_majuscule=deja_dit[-1]#on modifie la variable lettre_en_majuscule par le dernier élément de la liste de lettre deja dites (donc la dernière lettre qu'on a écrit
                decouv_lettre=decouvrir_lettre(deja_dit[-1],mot_myst,mot_a_decouvrir)
                if decouv_lettre==False: #si cette lettre ne fait pas parti du mot mystère
                        nb_err += 1 #on ajoute 1 aux nombre d'erreur commise par le joueur 
                        afficher_potence_texte(nb_err,nb_err_max)#on affiche la potence du mot "perdu"
                print(mot_a_decouvrir)# on écrit le mot à decouvrir avec les lettre découvert et les caractère de substitution
        if liste_mot_myst == mot_a_decouvrir: #si le mot à découvrir est entièrement découvert et il est égale à la liste de caractàre du mot mystère , on ecrit le mot "gagné"
                print("gagnée")
        else:
                print("perdu")#si ce n'est pas le cas , on écrit le mot perdu et on affiche le mot mystère qu'on devait découvrir
                print(mot_myst)
        return liste_mot_myst == mot_a_decouvrir # on retourne un booléen qui est True or False selon si on a découvert le mot ou non
        

def partie_humain_alea(nom_fichier,nb_err_max,car_subst="-"):#c'est un appel de plusieur fonction dont partie_humain
        listedemot = importer_mots(nom_fichier)#on crée la liste de mot à l'aide de la fonction importer_mots
        mot_myst=choisir_mot_alea(listedemot)#on choisit un mot aléatoire dans cette liste à l'aide de la fonction choisir_mot_alea
        valeur = partie_humain(mot_myst,nb_err_max,car_subst)#on lance la partie aléatoire et valeur vaudra True or False selon si on gagne ou perd
        return valeur


                        

def fabrique_liste_alphabet():#le but est de fabriqué une liste de l'alphabet dans l'ordre alphabétique
        liste_alphabet=[]#on initialise une liste de l'alphabet vide
        for i in range (65,91):#65 est la position de la lettre "A" majuscule dans la table ASCII et 91 est la postion+1 de la lettre "Z" majuscule dans la table ASCII. On met c 91 car on parcourt la boucle for jusqu'à 91 non inclus et "Z" vaut 90 dans la table ASCII.Ainsi on commence le parcours de la boucle for à 65 inclus pour la lettre "A" jusqu'à 91 exclus pour la lettre "Z"
                liste_alphabet.append(chr(i))#on ajoute les lettre correspondante au fur et à mesure selon la valeur de i dans la liste de l'alphabet
        return liste_alphabet#on retourne la liste de l'alphabet complet
                
def fabrique_liste_alea():#le but de cette fonction est de créer une liste d'alphabet mélanger.
        liste_alea = fabrique_liste_alphabet()#à l'aide de la fonction fabrique_liste_alphabet , on créer une liste d'alphabet non mélanger , cet à dire que l'élément 0 est "A" et l'élément 25 est "Z"
        random.shuffle(liste_alea)#shuffle va mélanger aléatoirement la liste créer donc notre liste sera complètement aléatoire
        return liste_alea# on retourne la liste aléatoire de l'alphabet

def dico_frequence(nom_fichier):#le but est de faire un dictionnaire sur la fréquence des lettres de l'alphabet dans la liste des mots.
        listedemot =importer_mots(nom_fichier)# on créer la liste de mot
        liste_alphabet=fabrique_liste_alphabet()# on fabrique la liste d'alphabet
        dico={}# on initialise un dictionnaire vide
        for i in liste_alphabet:# on parcourt la liste de l'alphabet(qui est donc dans l'ordre alphabétique)
                dico[i] = 0 # on initialise la valeur des clé(les lettres) à 0
        for i in listedemot:# on parcourt la liste des mot
                mot = i #la valeur de la variable mot sera donc le mot à la position i de la listedemot 
                for j in range (len(mot)):# on parcourt les caractère du mots
                        dico[mot[j]] +=1# pour chaque lettre du mot , on rajoute +1 dans le dictionnaire pour chaque lettre présente dans le mot.
        return dico# On retourne le dictionnaire de lettres et le nombre de lettre présente dans la liste de mot.


def lettre_la_plus_frequente(dico):#le but est de retourner la lettre la plus fréquente dans la liste de mot (on a la fonction dico_frequence qui a créer un dictionnaire avec les lettres et leur nombre d'apparition
        lettre_la_plus_frequente=''# on initialise une lettre fréquente avec un caractère vide.
        nb_de_fois_dapparition_de_cette_lettre=0# on initialise à 0 le nombre d'apparition de ce caractère
        for key in dico: # on parcourt le dictionnaire
                if dico[key]>=nb_de_fois_dapparition_de_cette_lettre:#la valeur des clé du dictionnaire est le nombre d'apparition de la lettre(une lettre correspond à une clé du dictionnaire)
                                                                        #si la valeur de la clé est supérieur ou égale à celle d'une lettre qui etait pour l'instant la plus fréquente , alors on remplace
                                                                        #cette lettre par la clé 
                        lettre_la_plus_frequente=key
                        nb_de_fois_dapparition_de_cette_lettre=dico[key]#on remplace la valeur du nombre d'apparition de la lettre précédente par la valeur de la clé de la lettre la plus fréquente actuellement
        return lettre_la_plus_frequente #on retourne la lettre la plus fréquente

                

def fabrique_liste_freq(nom_fichier):# le but est de créer une liste de lettre de l'alphabet classé des plus fréquente aux moins fréquentes.La valeur 0 de la liste sera donc la plus fréquente et la valeur 25 sera la lettre la moin fréquente.
        liste_de_la_plus_a_la_moin_frequente = []#liste vide qu'on mettra à jour avec les lettre les plus fréquente
        dico = dico_frequence(nom_fichier)#on créer le dictionnaire des lettres à l'aide de la fonction dico_frequence
        for i in range(26):#on parcourt le dictionnaire
                liste_de_la_plus_a_la_moin_frequente.append(lettre_la_plus_frequente(dico))#on ajoute la lettre la plus fréquente à la liste
                del dico[lettre_la_plus_frequente(dico)] #on supprime à chaque fois la lettre la plus frequente dans le dictionnaire
        return liste_de_la_plus_a_la_moin_frequente #on retourne la liste qui est donc trié dans l'ordre des lettre les plus fréquente aux moins fréquente.
        
def partie_auto(mot_myst,liste_lettres,affichage=True,pause=False):#cette fonction prend en argument le mot mystère mot_myst,une liste de lettre de l'alphabet et 2 argument optionnels affichage et pause.Le but de cette fonction est de faire deviner à l'ordinateur un mot choisis.Cette fonction retournera le nombre d'erreur lors de la partie effectué.
        nb_err=0# on initialise le nombre d'erreur effectué par l'ordinateur à zéro
        liste_mot_myst=list(mot_myst.upper())#On convertie le mot mystère en liste de caractère car quand on utilise la fonction initialiser_mot_part_decouv , la fonction retourne une liste.Ce n'est pas obligatoire de faire une liste de caractère mais je préfère le faire car c'est plus cohérent.
        mot_a_decouvrir=initialiser_mot_part_decouv(mot_myst)#mot de depart initilalisé.
        deja_dit=[]#on créer une liste vide de lettre déja dit par l'ordinateur qu'on mettra à jour.
        i=0#on initialise l'indice i à zéro .i va nous servir à parcourir la liste de l'alphabet.
        while liste_mot_myst != mot_a_decouvrir:#Tant que l'ordinateur ne trouve pas le mot mystère , il continue de chercher.
                deja_dit.append(liste_lettres[i])#on ajoute à la liste des lettres déja dit la lettre en position i de la liste de lettre de l'alphabet.
                lettre_en_majuscule=deja_dit[-1]#on convertie la dernière lettre choisit en lettre majuscule. L'indice -1 correspond au dernier élément de la liste de lettre deja_dit donc la dernière lettre dite.
                
              
                lettre_a_decouvrir=decouvrir_lettre(lettre_en_majuscule.upper(),mot_myst,mot_a_decouvrir)# on fait appelle à la fonction decouvrir_lettre qu'on a codé précédemment pour modifier par effet de bord le mot_a_decouvrir et aussi selon le booléen , ajoutez +1 au nombre d'erreur commise par l'ordinateur.Si l'ordinateur trouve une lettre , on modifie par effet de bord et le booléen vaudra True , sinon il vaudra False et donc on ajoutera +1 au nombre d'erreur commise par le pc
                if lettre_a_decouvrir == False:
                    nb_err+=1
                if affichage == True:#l'affichage sert à afficher le mot partiellement decouvert mot_a_decouvrir à chaque parcourt de la boucle while (donc à chaque lettre proposé par l'ordinateur)si on a choisit d'avoir l'affichage , si on a pas choisit alors on aura pas d'affichage.
                        print(mot_a_decouvrir)
                if pause == True:#si on a choisit d'avoir une pause , alors on en aura une à chaque parcourt de la boucle et on devra appuyez sur n'importe quel touche pour continuer la partie.
                        input("Appuyez sur n'importe quelle touche pour continuer")
                i +=1#mise à jour de l'indice.
        return nb_err#retourne le nombre d'erreur
        #à noter que dans le programme principal , on demande aux joueur si on souhaite laisser l'affichage et la pause ou non .Si le joueur souhaite laisser alors la variable affichage et la variabe pause vaudra True et si il veut pas alors la variable affichage et la variable pause vaudra False.De plus le joueur peut très bien laisser la pause mais ne pas laisser l'affichage et ainsi de suite.




#partie extension
def dico_frequence_longueur(nom_fichier,mot_myst):#cette fonction prend en argument un fichier nom_fichier et le mot mystère.Cette fonction s'inspire de la fonction dico_frequence et à pour but de faire un dictionnaire de la fréquence des lettres selon la longueur du mot mystère.Par exemple si le mot mystère a une longueur de 6 caractère alors on va établir un dictionnaire de fréquence de lettre dans les mot de longueur de 6 caractère.
        listedemot=importer_mots(nom_fichier)#on créer la liste de mot
        liste_alphabet=fabrique_liste_alphabet()#on fabrique la liste d'alphabet
        dico={}# on initialise un dictionnaire vide
        for i in liste_alphabet:#on parcourt la liste de l'alphabet(qui est donc dans l'ordre alphabétique)
                dico[i] = 0 # on initialise la valeur des clé(les lettres)à 0
        for i in listedemot:#on parcourt la liste des mots
                mot=i#la valeur de la variable mot sera donc le mot à la position i de la listedemot 
                if len(mot)==len(mot_myst):# en parcourant la liste de mot , si on tombe sur un mot de la meme longueur du mot mystère alors on va parcourir les caractère du mot ayant la même longueur que le mot mystère et on va rajouter +1 aux lettre correspondante dans le dictionnaire.Exemple si il y a la lettre a , b et c dans le mot alors on rajoute +1 à la valeur de la clé a , b et c.
                        for j in range(len(mot)):#on parcourt les caractères du mots
                                dico[mot[j]]+=1#pour chaque lettre du mot , on rajoute +1 dans le dictionnaire pour chaque présente dans le mot.
        return dico# On retourne le dictionnaire de lettres et le nombre de lettre présente dans la liste de mot.
def fabrique_liste_freq_v2(dico):#cette fonction est une copie de la fonction fabrique_liste_freq mais en prennant cette fois-ci en argument un dictionnaire(on suppose que c'est le dictionnaire du nombre d'apparition des lettres selon la longueur du mot mystère).Pour les explications de cette fonction, je vous invite à regarder les commentaires effecuter pour la fonction fabrique_liste_freq.
        liste_de_la_plus_a_la_moin_frequente = []
        dico2 = dict(dico)#on fait une copie du dictionnaire pour ne pas modifier le dictionnaire
        for i in range(26):#
                liste_de_la_plus_a_la_moin_frequente.append(lettre_la_plus_frequente(dico2))
                del dico2[lettre_la_plus_frequente(dico2)] 
        return liste_de_la_plus_a_la_moin_frequente 

def difficulte(nom_fichier,difficulter):#cette fonction prend en argument un fichier et un caractère.C'est la fonction importer_mots un peut revisité et qui a pour but de faire un système de niveau de difficulté dans le pendu . Plus on choisit une grande difficulté , plus le mot sera long.
        fichier=open(nom_fichier)#ouvre le fichier
        listedemot=[]#création d'une liste vide qu'on va mettre à jour avec les mot du fichier nom_fichier
        if difficulter=="f":#'f' est l'initiale de facile donc si on choisit le mode facile , on va remplir notre liste listedemot avec des mots qui sont "facile".
                for i in fichier: #on parcourt ligne par ligne le fichier
                        lignesansretour=i.strip()#on initialise la variable lignesansretour avec la ligne i du fichier (qui correspond à un mots car il y'a un mot par lignes
                        if len(lignesansretour)>=3 and len(lignesansretour)<7: #si le mot fait entre 3 et 6 caractère
                                listedemot.append(lignesansretour.upper())#on l'ajoute à la liste listedemot 
        elif difficulter=="m":#"m" correspond à la difficulté "moyen" , on reprend le même algo que pour la difficulté facile mais cette fois si avec les mots de longueur 7 à 10
                for i in fichier:
                        lignesansretour=i.strip()
                        if len(lignesansretour)>=7 and len(lignesansretour)<11:
                                listedemot.append(lignesansretour.upper())      
        elif difficulter=="d":#"d" correspond à "difficile",on reprend le même algo mais cette fois si avec les mots d'au moins 11 caractères.
                for i in fichier:
                        lignesansretour=i.strip()
                        if len(lignesansretour)>=11:
                                listedemot.append(lignesansretour.upper())
        elif difficulter=="a":#"a" correspond à la difficulté "aléatoire" et cette fois si on va faire appelle à la fonction importer_mots et donc on va prendre des mots de n'importe quelle tailles (qui font au moins 3 caractères
                listedemot=importer_mots(nom_fichier)
        return listedemot # on retourne la listedemot remplis selon la difficulté choisis.
        
#Menu du jeu
rejouer = "o"#on initialise une variable rejouer qui nous servira plus tard si on veut rejouer."o" signifie "oui"
input('Bienvenue sur le jeu "le pendu" codé par Kylian Alkaya, appuyez sur la touche Entrée pour continuer.')#petite présentation.
while rejouer == "o":#Tant que rejouer vaudra "o" , on pourra rejouer autant qu'on veut.
        pcouhumain=input("Voulez-vous une partie humain contre robot ou robot contre robot ? Ecrivez h pour une partie humain contre robot ou ecrivez r pour une partie robot contre robot")# on a le choix de choisir entre une partie robot contre robot et humain contre robot.
        while pcouhumain!="h" and pcouhumain !="r": #si on choisit pas la lettre h ou r , le robot ne comprendra pas alors il faudra recommencer jusqu'à qu'on ait bien mis la bonne réponse.
                pcouhumain=input("je n'ai pas compris , veuillez taper h pour une partie humain contre robot ou r pour une partie robot contre robot")
        if pcouhumain=='h':
                niveau=input("Vous avez le choix entre: \n-le niveau facile qui vous donnera un mot court \n-le niveau moyen qui vous donnera un mot moyen \n-le niveau difficile qui vous donnera un mot long \n-le niveau aleatoire qui choisira un mot de n'importe quelle niveau.\nEcrivez f pour un niveau facile , ecrivez m pour un mot de niveau moyen , ecrivez d pour un mot de niveau difficile ou ecrivez a pour un mot de niveau aleatoire")#on peut choisir entre 4 niveau de difficulté , a pour aleatoire , f pour facile , m pour moyen , d pour difficile.
                while niveau !="a" and niveau !="f" and niveau !="d" and niveau !="m":#si on se trompe , alors on devra recommencer jusqu'à qu'on mette une bonne réponse.
                        niveau = input("vous avez mal choisi votre niveau de difficulté , veuillez recommencez , pour rappel , vous avez le choix entre f pour facile , m pour moyen , d pour difficile , a pour aleatoire")
                nb_err=int(input("combien d'erreur max voulez-vous ?"))#on choisit le nombre d'erreur max qu'on peut effectuer durant une partie.
                listedemot=difficulte('mots.txt',niveau)#on remplit une liste de mot avec des mots d'une certaine longueur selon le niveau de difficulté choisis
                mot_myst=choisir_mot_alea(listedemot)#on choisit un mot aléatoire dans cette même liste.
                partie_humain(mot_myst,nb_err)#on fait la partie.
        elif pcouhumain=="r":
                niveau=input("Vous avez le choix entre : \n-le niveau facile qui vous donnera un mot court \n-le niveau moyen qui vous donnera un mot moyen \n-le niveau difficile qui vous donnera un mot long \n-le niveau aleatoire qui choisira un mot de n'importe quelle niveau.\nEcrivez f pour un niveau facile , ecrivez m pour un mot de niveau moyen , ecrivez d pour un mot de niveau difficile ou ecrivez a pour un mot de niveau aleatoire")
                while niveau !="a" and niveau !="f" and niveau !="d" and niveau !="m":
                        niveau = input("vous avez mal choisi votre niveau , veuillez recommencez , pour rappel , vous avez le choix entre f pour facile , m pour moyen , d pour difficile , a pour aleatoire")
                listedemot=difficulte('mots.txt',niveau)
                mot_myst=choisir_mot_alea(listedemot)
                strategie=input("vous pouvez choisir plusieurs stratégie pour le robot.Vous avez : \nla stratégie basique ,cet à dire que le robot va choisir les lettres dans l'ordre alphabétique \n-la stratégie aléatoire où le robot va choisir des lettres aléatoire de l'alphabet \n-la stratégie de la fréquence où le robot va choisir les lettres des plus fréquente à la moin fréquente dans notre liste de mots \n-la stratégie de la frequence v2 où le robot va choisir les lettres des plus fréquente à la moin fréquente mais en prennant en compte la longueur du mots.Exemple si un mot de 6 caractères est choisis , on va alors établir une liste des lettres des plus fréquente à la moins fréquentes pour tout les mots de 6 caractères.\nChoisissez la lettre b pour la stratégie basique ou la lettre a pour les lettre aléatoire ou f pour la technique des lettres fréquente ou la lettre g pour la technique des lettres fréquente v2.") #on nous demande de choisir entre la lettre b pour la stratégie , la lettre a pour la stratégie lettre aléatoire , la lettre f pour la stratégie des fréquences de lettres  et la lettre g pour la stratégie des fréquences de lettres selon la longueurs du mot.
                while strategie !="b" and strategie !="a" and strategie!="f" and strategie !="g":#si on choisit pas une des lettres demandé , on devra recommencer.
                        strategie=input("vous avez mal choisis la stratégie pour le robot , choisissez b pour la stratégie basique ou a pour la strategie aléatoire ou f pour la stratégie avec les lettres fréquentes.")
                if strategie =="b":
                        liste_alphabet = fabrique_liste_alphabet() # si on choisit la stratégie basique alors on créer une liste_alphabet à l'aide de la fonction fabrique_liste_alphabet et qui va donc créer une liste dans l'ordre alphabetique.
                elif strategie=="a":
                        liste_alphabet = fabrique_liste_alea()#pour la stratégie aléatoire , on va créer une liste d'alphabet à l'aide de la fonction fabrique_liste_alea qui va nous créer une liste de lettres de l'alphabet dans le désordre/
                elif strategie=="f":
                        liste_alphabet=fabrique_liste_freq("mots.txt") #pour la stratégie des fréquences de lettres , on va créer une liste d'alphabet à l'aide de la fonction fabrique_liste_freq qui prend en arguments notre fichier mots.txt et qui va créer une liste de lettres de l'alphabet rangé dans l'ordre des lettres les plus fréquentes aux moins fréquentes dans notre fichier.
                elif strategie=="g":
                        dico=dico_frequence_longueur("mots.txt",mot_myst)
                        liste_alphabet = fabrique_liste_freq_v2(dico)#pour la stratégie des fréquences de lettres selon la longueurs du mots , on va faire appelles aux fonction dico_frequence_longueur qui prend en argument le fichier mots.txt et le mot mystère et qui va créer un dictionnaire avec comme clé les lettres de l'alphabet et la valeur de ces clé sera le nombre d'apparition des lettres dans les mots du fichiers ayant la même longueur que le mot mystère. On va ensuite créer une liste de lettres de l'alphabet avec les lettres les plus fréquentes aux moins fréquentes selon la taille du mot.
                pause = input("pour chaque execution de lettres , voulez vous des pauses ou non ? Tapez o pour oui et n pour non")#on veut choisir si on veut une pause ou non entre chaque lettres écrite par le robot.On doit choisir entre la lettre o pour oui et n pour non.
                while pause !="o" and pause !="n":
                        pause = input("je n'ai pas compris , tapez o pour oui et n pour non")#Tant qu'on met autre chose que o et n , on doit recommencer.
                if pause=="o":
                        pause=True #pause vaudra True si on veut une pause et False si on en veut pas.
                elif pause=="n":
                        pause=False
                affichage = input("Voulez vous laisser l'affichage du mot avec la potence à chaque lettre inscrite ou non ? Tapez o pour oui et n pour non")#on va utiliser le même principe que pause mais cette fois-ci pour l'affichage du mots entre chaque lettre inscrite par le robot.
                while affichage !="o" and affichage !="n":
                        affichage=input("Je n'ai pas bien compris , tapez o pour laisser l'affichage et n pour ne pas laisser l'affichage")
                if affichage =="o":
                        affichage = True
                elif affichage =="n":
                        affichage = False
                print(partie_auto(mot_myst,liste_alphabet,affichage,pause))#on effectue la partie robot contre robot. Cette fonction prend en argument le mot mystère , la liste des lettres de l'alphabet , l'affichage et la pause.
                

        rejouer = input("Voulez vous rejouer ? tapez 'o' pour oui ou 'n' pour non")#on choisit si on veut rejouer 
        while rejouer !="o" and rejouer!="n":#tant qu'on met pas la bonne réponse alors on recommence.
                rejouer= input("je n'ais pas compris , voulez vous rejouez ? vous devez taper o pour oui ou n pour non")
